import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AppRoutingModule } from './app-routing.module';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service/in-memory-data.service';

import { AppComponent }         from './app.component/app.component';
import { DashboardComponent }   from './dashboard.component/dashboard.component';
import { ProductsComponent }      from './products.component/products.component';
import { ProductDetailComponent }  from './product-detail.component/product-detail.component';
import { ProductService }          from './product.service/product.service';
import { ProductSearchComponent }  from './product-search.component/product-search.component';
import { NewProductComponent }  from './new-product.component/new-product.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    // InMemoryWebApiModule.forRoot(InMemoryDataService),
    AppRoutingModule,
    AngularFontAwesomeModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    ProductDetailComponent,
    ProductsComponent,
    ProductSearchComponent,
    NewProductComponent
  ],
  providers: [ ProductService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
