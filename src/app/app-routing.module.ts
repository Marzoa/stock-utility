import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// -- import app components now

import { DashboardComponent }   from './dashboard.component/dashboard.component';
import { ProductsComponent }      from './products.component/products.component';
import { ProductDetailComponent }  from './product-detail.component/product-detail.component';
import { NewProductComponent }  from './new-product.component/new-product.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard',  component: DashboardComponent },
  { path: 'detail/:id', component: ProductDetailComponent },
  { path: 'products',     component: ProductsComponent },
  { path: 'newProduct',     component: NewProductComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
