import { Injectable }    from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable }     from 'rxjs/Observable';  

 
import 'rxjs/add/operator/toPromise';
 
import { Product } from '../product/product';
 
@Injectable()
export class ProductService {
 
  private headers = new Headers({'Content-Type': 'application/json'});
  private productsUrl = 'api/products';  // URL to web api
 
  constructor(private http: Http) { }
 
  getProducts(): Observable<Product[]> {
    return this.http.get(this.productsUrl)
               .map(response => response.json().data as Product[])
               .catch(this.handleError);
  } 

  getProduct(id: number): Observable<Product> {
    const url = `${this.productsUrl}/${id}`;
    return this.http.get(url)
      .map(response => response.json().data as Product)
      .catch(this.handleError);
  }
 
  deleteProduct(id: number): Promise<void> {
    const url = `${this.productsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
 
  createProduct(name: string, description: string, units: number): Observable<Product> {
      return this.http
        .post(this.productsUrl, JSON.stringify({name: name, description: description, units: units}), {headers: this.headers})
        .map(res => res.json().data as Product)
        .catch(this.handleError);    
  }
 
  updateProduct(product: Product): Observable<Product> {
    const url = `${this.productsUrl}/${product.id}`;
    return this.http
      .patch(url, JSON.stringify(product), {headers: this.headers})
      .map(() => product)
      .catch(this.handleError);
  }
 
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}