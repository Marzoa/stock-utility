"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InMemoryDataService = (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        var products = [
            { id: 0, name: 'Patata', description: 'Blanca', units: 10 },
            { id: 1, name: 'Cebolla', description: 'Roja', units: 50 },
            { id: 2, name: 'Ajo', description: 'Negro', units: 25 }
        ];
        return { products: products };
    };
    return InMemoryDataService;
}());
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map