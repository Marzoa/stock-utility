//creacion de la clase product
export class Product {
  id: number;
  name: string;
  description: string;
  units: number;
}
