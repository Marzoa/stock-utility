import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
 
import { Product }                from '../product/product';
import { ProductService }         from '../product.service/product.service';
 
@Component({
  selector: 'newProduct',
  templateUrl: './new-product.component.html',
  styleUrls: [ './new-product.component.css' ]
})
export class NewProductComponent {
  products: Product[];
  selectedProduct: Product;
 
  constructor(
    private productService: ProductService,
    private router: Router) { }
 
  getProduct(): void {
    this.productService
        .getProducts()
        .subscribe(products => this.products = products);
  }

  gotoProducts(): void {
    this.router.navigate(['/products']);
  }

  add(name: string, description: string, units: number): void {
    name = name.trim();
    description = description.trim();    
    if (!name || !units) { return; };
    this.productService.createProduct(name, description, units)
      .subscribe(product => {
        this.products=[]; //need to start an empty array 
        this.products.push(product);
      });
    this.gotoProducts();
  }

}
