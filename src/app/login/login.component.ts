import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) {}
  ngOnInit() {

  }

  login(form: NgForm) {
    if(form.value.username === 'root' && form.value.password === 'abc123..') {
      console.log(form.value)
      localStorage.removeItem('username');
      localStorage.setItem('username', form.value.username);
      this.router.navigate(['/dashboard']);
    } else if(form.value.username === 'visitant' && form.value.password === 'abc123..') {
      localStorage.removeItem('username');
      localStorage.setItem('username', form.value.username);
      this.router.navigate(['/dashboard']);
    } else {
      window.alert('Bad credentials');
    }
  }

}
