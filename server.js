const services = require('./src/api/services.js');
const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

// Run the app by serving the static files
// in the dist directory
app.use(express.static(__dirname + '/dist'));


// If an incoming request uses
// a protocol other than HTTPS,
// redirect that request to the
// same url but with HTTPS
const forceSSL = function() {
    return function (req, res, next) {
      if (req.headers['x-forwarded-proto'] !== 'https') {
        return res.redirect(
         ['https://', req.get('Host'), req.url].join('')
        );
      }
      next();
    }
}
  
// Instruct the app
// to use the forceSSL
// middleware
// app.use(forceSSL());

// Use Parser
app.use(bodyParser.json());

// Here the API endpoints
// All products
app.get('/api/products', function(req, res) {
  services.getProducts()
  .then(function(data){
    res.send(data)
  })
  .catch(function(err){
    console.log(err);
    res.status(500);
    res.send(err);
  })
})

// One product
app.get('/api/products/:id', function(req, res) {
  services.getProduct(req.params.id)
  .then(function(data){
    res.send(data)
  })
  .catch(function(err){
    console.log(err);
    res.status(500);
    res.send(err);
  })
})

// Delete product
app.delete('/api/products/:id', function(req, res) {
  services.deleteProduct(req.params.id)
  .then(function(){
    res.send({})
  })
  .catch(function(err){
    console.log(err);
    res.status(500);
    res.send(err);
  })
})

// Create product, sería guay devolver el product
app.post('/api/products', function(req, res) {
  services.createProduct(req.body) //se pasa el objeto body a services.js
  .then(function(data){
    res.send(data)
  })
  .catch(function(err){
    console.log(err);
    res.status(500);
    res.send(err);
  })
})

// Update product
app.patch('/api/products/:id', function(req, res) {
  services.updateProduct(req.params.id, req.body)
  .then(function(data){
    res.send(data)
  })
  .catch(function(err){
    console.log(err);
    res.status(500);
    res.send(err);
  })
})


// For all GET requests, send back index.html
// so that PathLocationStrategy can be used
app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});

// Start the app by listening on the default
// Heroku port
app.listen(process.env.PORT || 8080);
console.log('Server listening on port:', process.env.PORT || 8080);